import json
import pdb
import pe_enes as ps
from dispel4py.workflow_graph import WorkflowGraph
import sys
from collections import OrderedDict

map_proc_elem = {"0":"PreProcess_multiple_scenario",
"1":"IcclimProcessing",
"2":"StreamProducer",
"3":"NetCDF2xarray",
"4":"ReadNetCDF",
"5":"StandardDeviation",
"6":"AverageData",
"7":"CombineScenario",
"8":"PlotMultipleScenario",
"9":"B2DROP"}

#TODO add a proc elem to create netcdf for standarddeviation and AverageData

src_to_dest = {"0":[1],
		"1":[1,5,6,9],
		"2":[1],
		"3":[None],
		"4":[5,6],
		"5":[7],
		"6":[7],
		"7":[8],
		"8":[9]}

prev_proc_required = {"0":[None],
		"1":[0],
		"2":[None],
		"3":[None],
		"4":[1],
		"5":[4],
		"6":[4],
		"7":[5,6],
		"8":[7]
}

class Climate_Workflow(WorkflowGraph):

    def __init__(self, param):
        
        WorkflowGraph.__init__(self)
        self.param = param
        self.preprocess = None
        self.num_block = 1
        self.nb_block = len(param)
        self.combine_proc_elem = None

        #Check B2DROP id
        param_workflow = self.param['Workflow'][0]
        list_param_workflow = param_workflow.keys()
        #b2drop = [i for i in [*param_workflow] if 'B2DROP' in i]
        b2drop = [i for i in list_param_workflow if 'B2DROP' in i]
        if b2drop:
            self.b2drop_id = param_workflow[b2drop[0]]

    def create_workflow(self, **kwargs):
        
        param_workflow = self.param['PE']

        if sys.version[0]=='3':
            name_first_pe = [*self.param][0]
            param_workflow_keys = [*param_workflow]
        else:
            name_first_pe = self.param.keys()[0]
            param_workflow_keys = param_workflow.keys()

        param = OrderedDict()

        for block in param_workflow_keys:
            param[block] = OrderedDict()
            if sys.version[0]=='3':
                list_node = [*param_workflow[block]]
            else:
                list_node = param_workflow[block].keys()
            list_node.sort()
            for node in list_node:
                param[block][node] = param_workflow[block][node]

        #Below line only works for python3
        #name_first_pe = [*self.param][0]
        prev_proc_elem = self.preprocess
        
        #Below line only works for python3
        #list_kwargs = [*kwargs]
        list_kwargs = kwargs.keys()

        if 'scenario' in kwargs:
            scenario_name = kwargs['scenario']
        else:
            scenario_name = ""

        num_block=self.num_block

        if sys.version[0]=='3':
            list_block=[*param] 
        else:
            list_block=param.keys()
        block = list_block[num_block-1]

        nb_nodes = len(param[block])
        num_node = 1
        list_proc_elem = []
        
        for node in param[block]:

            if num_block>1 and num_node==1:
                prev_proc_elem = self.combine_proc_elem
                num_node+=1
                continue
            
            elif num_block==1 and num_node==1:
                prev_prov_elem=self.preprocess
            
            for proc_elem in param[block][node]:
                
                if num_node>1 and list_proc_elem:
                    prev_proc_elem = list_proc_elem[0]

                name_proc_elem = '{0}_{1}_{2}'.format(node,proc_elem[:-2],scenario_name)
                
                try:
                    if proc_elem=='B2DROP()':
                        exec(name_proc_elem+"=ps."+proc_elem[:-2]+"(self.b2drop_id)")
                        exec(name_proc_elem+".name=name_proc_elem")
                    else:
                        exec(name_proc_elem+"=ps."+proc_elem)
                        exec(name_proc_elem+".name=name_proc_elem")
                except AssertionError as error:
                    print(error)
                
                #This condition aims to connect the last node of one block with the next block
                if num_node==nb_nodes and num_block<self.nb_block:
                    self.connect(eval(name_proc_elem), 'output', self.combine_proc_elem, scenario_name)

                list_proc_elem.append(eval(name_proc_elem))

                #print('{0}  {1}'.format(prev_proc_elem.name, name_proc_elem))

                self.connect(prev_proc_elem, 'output', eval(name_proc_elem), 'input')

            num_node+=1



class Multiple_scenario(Climate_Workflow):

    def __init__(self, param):
        Climate_Workflow.__init__(self, param)
        #Below line only works for python3

        if sys.version[0]=='3':
            name_first_node = [*self.param['Workflow'][0]][0]
        else:
            name_first_node = self.param['Workflow'][0].keys()[0]
        self.nb_scenario = len(self.param['Workflow'][0][name_first_node]['in_files'])
        self.combine_proc_elem = None
        self.multiple_scenario_ = True
        self.nb_block = len(self.param['PE'])

    def multiple_scenario(self):
        #Main preprocessing element 
        preprocess = ps.PreProcess_multiple_scenario(self.param, self.nb_scenario)
        preprocess.name = "Workflow"
        self.preprocess = preprocess

        #Processing element to combine the multiple scenario
        if self.nb_block>1:
            combine_proc_elem = ps.CombineScenario(self.nb_scenario)
            combine_proc_elem.name = "combine_scenario"
            self.combine_proc_elem = combine_proc_elem
        
        param_workflow = self.param['PE']

        for block in param_workflow:

            if block=='Block_1':	
                for scenario in range(self.nb_scenario):
                    scenario_name = "scenario_"+str(scenario+1)
                    kwargs = {'scenario':scenario_name}
                    self.create_workflow(**kwargs)
                   
            elif block=='Block_2':
                self.create_workflow()	

            self.num_block+=1
