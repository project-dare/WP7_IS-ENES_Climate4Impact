'''
Example of code to run a simple process using the multiple_scenario instance from preprocess
'''

import pdb
import json
from PreProcess import Multiple_scenario

json_path = 'input_C4I.json'
with open(json_path) as json_data:
	input_C4I = json.load(json_data)

clim_workflow = Multiple_scenario(param=input_C4I)
clim_workflow.multiple_scenario()

from dispel4py.new import simple_process
result = simple_process.process_and_return(clim_workflow, input_C4I)

from dispel4py.workflow_graph import drawDot
with open('graph_multiple_scenario.png', 'wb') as f:
   f.write(drawDot(clim_workflow))

