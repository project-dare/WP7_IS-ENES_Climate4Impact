import json
import pdb
import pe_enes as ps
from dispel4py.workflow_graph import WorkflowGraph

class Climate_Workflow():

    def __init__(self, param):
        self.graph = WorkflowGraph()
        self.proc_elem = None

    def build_block(self, param):
        for pe in param:
            try:
                exec(name_scenario+"=ps."+proc_elem)
                exec(name_scenario+".name=name_scenario")
            except AssertionError as error:
                print(error)

    def multiple_scenario(self, param):
        for scenario in range(nb_scenario):
            self.graph.connect(preprocess, 'output', eval(name_scenario), 'input')
    #def add_block(self, ):



class multiple_scenario(Climate_Workflow):
    def __init__(self):
        Climate_Workflow.__init__(self)


def build_block(self, proc_elem, name_scenario):
    try:
        exec(name_scenario+"=ps."+proc_elem)
        exec(name_scenario+".name=name_scenario")
    except AssertionError as error:
        print(error)

    graph.connect(preprocess, 'output', eval(name_scenario), 'input')

def get_proc_elem_from_dict(PE):
    pdb.set_trace()

def print_something(argu):
    print(argu)

def build_graph_multiple_scenario(json_path):

    with open(json_path) as json_data:
        input_C4I = json.load(json_data)
        
    name_pe = [*input_C4I['PE']]
    pe_class = input_C4I['PE']
    name_pe.sort()

    #Create the PE defined by JSON files from C4I
    nb_scenario = len(input_C4I['Workflow'][0]['PE1_IcclimProcessing']['in_files'])
    name_scenario_list = []
    pdb.set_trace()
    #get_proc_elem_from_dict(PE)

    graph = WorkflowGraph()

    preprocess = ps.PreProcess_multiple_scenario()
    preprocess.name = "Workflow"

    for scenario in range(nb_scenario):

        name_scenario = "scenario_"+str(scenario+1)
        name_scenario_list.append(name_scenario)
        mean_calc=True
        std_calc=True

        try:
            exec(name_scenario+"=ps.IcclimProcessing()")
            exec(name_scenario+".name=name_scenario")
        except AssertionError as error:
            print(error)

        graph.connect(preprocess, 'output', eval(name_scenario), 'input')


        if mean_calc:
            read_pe = "read_"+name_scenario
            try:
                exec(read_pe+"=ps.ReadNetCDF()")
                exec(read_pe+".name=read_pe")
            except AssertionError as error:
                print(error)

            graph.connect(eval(name_scenario), 'output', eval(read_pe), 'input')

            mean_pe = "average_"+name_scenario
            try:
                exec(mean_pe+"=ps.AverageData()")
                exec(mean_pe+".name=mean_pe")
            except AssertionError as error:
                print(error)

            graph.connect(eval(read_pe), 'output', eval(mean_pe), 'input')

            if scenario==0:
                combine_mean = ps.CombineScenario(nb_scenario)
                combine_mean.name = "su_combine_mean"  

                plot_mean = ps.PlotMultipleScenario()
                plot_mean.name = "average_all_scenario"

            graph.connect(eval(mean_pe), 'output', combine_mean, name_scenario)
            graph.connect(combine_mean, 'combined_data', plot_mean, 'TimeSeries')


        if std_calc:
            read_pe = "read_"+name_scenario
            try:
                exec(read_pe+"=ps.ReadNetCDF()")
                exec(read_pe+".name=read_pe")
            except AssertionError as error:
                print(error)

            graph.connect(eval(name_scenario), 'output', eval(read_pe), 'input')

            std_pe = "std_"+name_scenario
            try:
                exec(std_pe+"=ps.StandardDeviation()")
                exec(std_pe+".name=std_pe")
            except AssertionError as error:
                print(error)

            graph.connect(eval(read_pe), 'output', eval(std_pe), 'input')
            if scenario==0:
                combine_std = ps.CombineScenario(nb_scenario)
                combine_std.name = "su_plot_std"
            

    
    return graph

def create_multiple_scenario_workflow():

    ###############################
    #Stream Producer, it will scan the json file in input
    ###############################
    streamProducer = ps.StreamProducer()
    streamProducer.name = 'SU_workflow'


    ###############################
    #Workflow for r1i2p1 simulation
    ###############################
    su_calculation_r1i2p1 = ps.NetCDFProcessing()
    su_calculation_r1i2p1.name = 'SU_calculation_r1i2p1'

    read_r1i2p1 = ps.ReadNetCDF()
    read_r1i2p1.name = "read_SU_r1i2p1"

    mean_calculation_r1i2p1 = ps.AverageData()
    mean_calculation_r1i2p1.name = "mean_r1i2p1"

    std_calc_r1i2p1 = ps.StandardDeviation()
    std_calc_r1i2p1.name = "std_r1i2p1"


    ###############################
    #Workflow for r2i2p1 simulation
    ###############################    
    su_calculation_r2i2p1 = ps.NetCDFProcessing()
    su_calculation_r2i2p1.name = 'SU_calculation_r2i2p1'

    read_r2i2p1 = ps.ReadNetCDF()
    read_r2i2p1.name = "read_SU_r2i2p1"

    mean_calculation_r2i2p1 = ps.AverageData()
    mean_calculation_r2i2p1.name = "mean_r2i2p1"

    std_calc_r2i2p1 = ps.StandardDeviation()
    std_calc_r2i2p1.name = "std_r2i2p1"


    ###############################
    #Workflow for r3i2p1 simulation
    ###############################    
    su_calculation_r3i2p1 = ps.NetCDFProcessing()
    su_calculation_r3i2p1.name = 'SU_calculation_r3i2p1'

    read_r3i2p1 = ps.ReadNetCDF()
    read_r3i2p1.name = "read_SU_r3i2p1"

    mean_calculation_r3i2p1 = ps.AverageData()
    mean_calculation_r3i2p1.name = "mean_r3i2p1"

    std_calc_r3i2p1 = ps.StandardDeviation()
    std_calc_r3i2p1.name = "std_r3i2p1"


    ###############################
    #Workflow to combine and plot the calculation together
    ###############################
    combine_std = ps.CombineAndPlot()
    combine_std.name = "SU_Spatial_STD"   

    combine_mean = ps.CombineAndPlot()
    combine_mean.name = "SU_Spatial_MEAN"   


    ###############################
    #Workflow to combine and plot the calculation together
    ###############################   
    b2drop = ps.B2DROP()
    b2drop.name = "b2drop_storage"

    ###############################
    #Workflow starts here
    ###############################
    graph = WorkflowGraph()

    #Calculation for r1i2p1
    graph.connect(streamProducer, 'output', su_calculation_r1i2p1, 'input')
    graph.connect(su_calculation_r1i2p1, 'output', read_r1i2p1, 'input')
    graph.connect(read_r1i2p1, 'output', mean_calculation_r1i2p1, 'input')
    graph.connect(read_r1i2p1, 'output', std_calc_r1i2p1, 'input')

    #Calculation for r2i2p1
    graph.connect(streamProducer, 'output', su_calculation_r2i2p1, 'input')
    graph.connect(su_calculation_r2i2p1, 'output', read_r2i2p1, 'input')  
    graph.connect(read_r2i2p1, 'output', mean_calculation_r2i2p1, 'input')
    graph.connect(read_r2i2p1, 'output', std_calc_r2i2p1, 'input')

    #Calculation for r3i2p1
    graph.connect(streamProducer, 'output', su_calculation_r3i2p1, 'input')
    graph.connect(su_calculation_r3i2p1, 'output', read_r3i2p1, 'input')
    graph.connect(read_r3i2p1, 'output', mean_calculation_r3i2p1, 'input')
    graph.connect(read_r3i2p1, 'output', std_calc_r3i2p1, 'input')

    ###############################
    #We combine the standard deviation and plot it together. Same for the average.
    ###############################
    graph.connect(std_calc_r1i2p1, 'output', combine_std, 'var1')
    graph.connect(std_calc_r2i2p1, 'output', combine_std, 'var2')
    graph.connect(std_calc_r3i2p1, 'output', combine_std, 'var3')

    graph.connect(mean_calculation_r1i2p1, 'output', combine_mean, 'var1')
    graph.connect(mean_calculation_r2i2p1, 'output', combine_mean, 'var2')
    graph.connect(mean_calculation_r3i2p1, 'output', combine_mean, 'var3')

    ###############################
    #We store all the results on b2drop
    ###############################
    graph.connect(combine_std, 'output', b2drop, 'input')
    graph.connect(combine_mean, 'output', b2drop, 'input')

    return graph