from pywps.Process.Process import WPSProcess   

from os.path import expanduser
from mkdir_p import *
from datetime import datetime
home = expanduser("~")
import json
#import PreProcess as pps
import io
from icclim import icclim
from icclim.util import read
import logging;
logging.debug("Something has been debugged")
from collections import OrderedDict

try:
    to_unicode = unicode
except NameError:
    to_unicode = str


class Process(WPSProcess):
    icclim_indice = read.get_icclim_indice_config()
    icclim_slice_mode = read.get_icclim_slice_mode()

    def __init__(self, icclim_indice=icclim_indice, icclim_slice_mode=icclim_slice_mode):
        # init process
        WPSProcess.__init__(self,
                            identifier="multiple_scenario_usecase", 
                            title="Multiple scenario workflow powered by dispel4py",
                            abstract="Build a workflow to perform indice calculation powered by icclim",
                            version = "1.0",
                            storeSupported = True,
                            statusSupported = True,
                            grassLocation =False)

        self.calc_operation = self.addLiteralInput(identifier = 'Calculation',
                                                     title = 'First Indice Calculation',
                                                     type = "String",
                                                     default = "SU") 

        self.calc_operation.values = icclim_indice

        self.input_varName = self.addLiteralInput(identifier = 'Input Var Name',
                                                     title = 'Var Name in netcdf',
                                                     type = type("String"),
                                                     default = "tas") 


        self.infile1 = self.addLiteralInput(identifier = 'NetCDF InputFile 1',
                                                title = 'File 1',
                                                abstract = 'Hola',
                                                type = type("String"),
                                                minOccurs=0,
                                                maxOccurs=1024,
                                                default = "https://esgdata.gfdl.noaa.gov/thredds/dodsC/gfdl_dataroot4/CMIP/NOAA-GFDL/GFDL-CM4/historical/r1i1p1f1/day/tas/gr1/v20180701/tas_day_GFDL-CM4_historical_r1i1p1f1_gr1_20100101-20141231.nc,\
https://esgdata.gfdl.noaa.gov/thredds/dodsC/gfdl_dataroot4/CMIP/NOAA-GFDL/GFDL-CM4/historical/r1i1p1f1/day/tas/gr2/v20180701/tas_day_GFDL-CM4_historical_r1i1p1f1_gr2_20100101-20141231.nc")    


        self.slicemode = self.addLiteralInput(identifier = 'Slice Mode',
                                                     title = 'Temporal Slice Mode',
                                                     type = type("String"),
                                                     default = "year")   

        self.slicemode.values = icclim_slice_mode

        self.proc_elem_2 = self.addLiteralInput(identifier = 'Second Processing Element',
                                                     title = 'Second Processing Element',
                                                     type = type("String"),
                                                     default = "AverageData")

                                                    

        self.proc_elem_3 = self.addLiteralInput(identifier = 'Third Processing Element',
                                                     title = 'Third Processing Element',
                                                     type = type("String"),
                                                     default = "CombineScenario")                                                        

        self.proc_elem_4 = self.addLiteralInput(identifier = 'Fourth Processing Element',
                                                     title = 'Fourth Processing Element',
                                                     type = type("String"),
                                                     default = "PlotMultipleScenario")


        self.B2DROP_username = self.addLiteralInput(identifier = 'B2DROP_user_id',
                                                     title = 'B2DROP_user_id',
                                                     abstract = 'First input for B2DROP user id, second input for the password id. \nEnter None value for one input if you do not want to store the output in B2DROP',
                                                     type = type("String"),
                                                     minOccurs=2,
                                                     maxOccurs=2,
                                                     default = "7f64f56c-a286-48fe-bf74-96567edef0d2,\
wWEqq-Qondr-7ZmqZ-ZRKrs-idDHJ")


    def callback(self,message,percentage):
        self.status.set("Processing: [%s]" % message,percentage);
        
    def execute(self):
        import json
        PATH_SYSTEM = '/src/wpsprocesses/impactwps-master/'
        FOLDER_B2DROP = 'enes_usecase'
                        
        def callback(a,b):
          self.callback(a,b)
        import os
        tmpFolderPath=os.getcwd()
        os.chdir(home)

        import time

        self.status.set("Preparing the set up", 0)
        time.sleep(0.2)
        pathToAppendToOutputDirectory = "/WPS_"+self.identifier+"_" + datetime.now().strftime("%Y%m%dT%H%M%SZ")
        
        """ URL output path """
        fileOutURL  = os.environ['POF_OUTPUT_URL']  + pathToAppendToOutputDirectory+"/"
        
        """ Internal output path"""
        fileOutPath = os.environ['POF_OUTPUT_PATH']  + pathToAppendToOutputDirectory +"/"

        """ Create output directory """
        mkdir_p(fileOutPath)

        """ Get output filename """

        def merge_two_dicts(x, y):
            z = x.copy()   # start with x's keys and values
            z.update(y)    # modifies z with y's keys and values & returns None
            return z

        #config_structure["icclim"]["indice"].keys()
        #Loading the config structure for dispel4py from config_indice.json in icclim library
        conf_filename_path = '/tmp/multiple_scenario.json'

        config_dict = OrderedDict()

        config_dict["Workflow"] = OrderedDict() 

        #config_dict["Workflow"]= [self.infile1.getValue()]

        config_workflow = OrderedDict()

        in_files_list = []

        for i in range(len(self.infile1.getValue())):
            in_files_list.append([self.infile1.getValue()[i]])

        config_workflow["Node_1_IcclimProcessing"] = {"out_file": None,
            "slice_mode": self.slicemode.getValue(),
            "user_indice": None,
            "indice_name": self.calc_operation.getValue(),
            "in_files": in_files_list,
            "var_name": self.input_varName.getValue()}

        #Check if we need to create differents blocks
        list_proc_elem = [self.proc_elem_2.getValue(), self.proc_elem_3.getValue(), self.proc_elem_4.getValue()]


        ind_combine = list_proc_elem.index("CombineScenario")

        config_node = OrderedDict()
        config_node["Block_1"] = OrderedDict()
        config_node['Block_1']['Node_1'] = ["IcclimProcessing()"]

        num_block = 1
        num_node = 2

        for proc_e in list_proc_elem:
            if num_node-2==ind_combine:
                num_block+=1
                config_node['Block_'+str(num_block)] = OrderedDict()

            config_node['Block_'+str(num_block)]['Node_'+str(num_node)] = [proc_e+"()"]
            num_node+=1

        if self.B2DROP_username.getValue():
            b2drop_username = self.B2DROP_username.getValue()
            config_workflow["Node_5_B2DROP"]= {"username": b2drop_username[0],
            "password": b2drop_username[1]}

            config_node["Block_"+str(num_block)]['Node_'+str(num_node)] = ["B2DROP()"]

        config_dict["Workflow"] = [config_workflow]
        config_dict["PE"] = config_node


        with open(conf_filename_path, 'w') as outfile:  
            json.dump(config_dict, outfile)

        #Update the call back
        for i in range(4):
            self.status.set("Json successfully created", i+1)
            time.sleep(0.1)

        with open(conf_filename_path) as inputfile:
            input_data = json.load(inputfile)

        import time

        total_operation = len(in_files_list)*(len(config_node["Block_1"]))
        if len(config_node)>1:
            total_operation += len(config_node["Block_2"])

        first_line = "graph = Multiple_scenario(param=input_data)"
        second_line = "graph.multiple_scenario()"

        code_2_add = '\ninput_data = {0}\n \n{1} \n{2}'.format(config_dict,first_line,second_line) 

        import shutil
        src_file = PATH_SYSTEM+'generic_workflow.py'
        dest_file = 'generic_workflow.py'
        shutil.copyfile(src_file, dest_file)

        with open(dest_file, "a") as myfile:
            myfile.write(code_2_add)
            

        ###############################
        #Calling the dare API
        ###############################  
        self.status.set("Creating dare-api workflow", 6)

        # Constant hostnames of exec-api and d4p-registry api
        EXEC_API_HOSTNAME = 'https://testbed.project-dare.eu/exec-api'
        D4P_REGISTRY_HOSTNAME = 'https://testbed.project-dare.eu/d4p-registry'

        # D4P-registry credentials
        REG_USERNAME = 'root'
        REG_PASSWORD = 'root'
        WORKSPACE_NAME = "WP7_Workspace"
        PE_URL_NAME="generic_workflow"
        UPLOAD_PATH="wp7-input"
        JSON_NAME = "input_C4I.json"

        # Imports
        import requests
        import helper_functions as F
        import pdb
        import json, os

        auth_token = F.login(REG_USERNAME, REG_PASSWORD, D4P_REGISTRY_HOSTNAME)
        header = F.get_auth_header(auth_token)
        print(auth_token, header)

        self.status.set("Successfull login to the testbed dare-api", 8)


        # Write token and hostnames to json
        creds = {}
        creds['D4P_REGISTRY_HOSTNAME'] = D4P_REGISTRY_HOSTNAME
        creds['EXEC_API_HOSTNAME'] = EXEC_API_HOSTNAME
        creds['header'] = header
        creds['REG_USERNAME'] = REG_USERNAME
        creds['REG_PASSWORD'] = REG_PASSWORD

        try:
            (workspace_url, workspace_id), status = F.create_workspace("", WORKSPACE_NAME, "", creds)
            workspace_id = int(workspace_id)
        except:
            F.delete_workspace(WORKSPACE_NAME, creds)
            workspace_url, workspace_id = F.create_workspace("", WORKSPACE_NAME, "", creds)
            workspace_id = int(workspace_id)

        self.status.set("Workspace {0} has been created".format(workspace_url), 10)

        pe_url = F.create_pe(desc="", name=PE_URL_NAME, conn=[], pckg=WORKSPACE_NAME.lower(),
                    workspace=workspace_url, clone="", peimpls=[], creds=creds)

        self.status.set("Url for the PE created as {0}".format(workspace_url), 12)

        impl_id = F.create_peimpl(desc="", code=open(dest_file).read(),
                                    parent_sig=pe_url, pckg=WORKSPACE_NAME.lower()+"_impl",
                                    name=PE_URL_NAME+"_impl", workspace=workspace_url,
                                    clone="", creds=creds)

        self.status.set("Code successfully sent to the worklow", 14)

        os.system('zip -r /src/wpsprocesses/impactwps-master/input_C4I.zip /src/wpsprocesses/impactwps-master/input_C4I.json')

        resp = F.myfiles(token=F.auth(), creds=creds)
        #upload_path, exec_path = F.create_new_upload_path(json.loads(resp), UPLOAD_PATH)

        upload_file, exec_path = F.find_upload_path(json.loads(resp), UPLOAD_PATH)
        exec_path = exec_path+"/"+JSON_NAME

        F.upload(token=F.auth(), path=UPLOAD_PATH, local_path=PATH_SYSTEM+'input_C4I.zip', creds=creds)

        self.status.set("Input file loaded to the testbed", 15)
        time.sleep(0.2)
        F.submit_d4p(impl_id=impl_id, pckg=WORKSPACE_NAME.lower(), workspace_id=workspace_id, pe_name=PE_URL_NAME,
                token=F.auth(),
                creds=creds,
                reqs='https://raw.githubusercontent.com/pagecp/DARE-Project/master/requirements.txt',
                inputfile=exec_path,
                target='simple', n_nodes=8, no_processes=8, iterations=1)

        self.status.set("Workflow is running on the testbed", 16)

        iteration = 0

        while True:
            resp = F.my_pods(token=F.auth(), creds=creds)
            test_ = [worker["status"] for i, worker in enumerate(json.loads(resp))]
            total_working_node = len(test_)
            self.status.set('Containers Pending: {0} // Containers Running: {1}'.format(test_.count('Pending'), test_.count('Running')), 16+iteration)    
            #pod_pretty_print(json.loads(resp))
            time.sleep(2) 
            if iteration<65:
                iteration+=1
            if not json.loads(resp) and iteration>10:
                break

        time.sleep(1)

        if self.B2DROP_username.getValue():
            self.status.set("Downloading file from b2drop", 98)
            import owncloud
            username = b2drop_username[0]
            password = b2drop_username[1]
            oc = owncloud.Client('https://b2drop.eudat.eu')
            oc.login(username, password)

            #for i in range(len(in_files_list)):
            #    oc.get_file('enes_usecase/scenario_'+str(i)+'.nc', fileOutPath+'/scenario_'+str(i)+'.nc')
     

        self.status.set("WPS successful", 99)

        time.sleep(2)        

        self.status.set("Finished....", 100)

        time.sleep(4)
