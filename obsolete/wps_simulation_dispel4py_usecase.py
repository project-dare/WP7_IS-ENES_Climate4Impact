from icclim import icclim
from icclim.util import read
from collections import OrderedDict
import pdb
import json
import PreProcess as pps

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

#Loading the config structure for dispel4py from config_indice.json in icclim library
conf_filename, json_structure = read.get_disp4py_config()
config_dict = OrderedDict(json_structure)

config_dict["Workflow"] = OrderedDict() 
nb_PE = 3
for i in range(nb_PE):
    if i+1<nb_PE:
        config_dict["PE"]["PE"+str(i+1)] = "NetCDFProcessing()"
        config_dict["Workflow"]["PE"+str(i+1)+"_NetCDFProcessing"] = OrderedDict(read.get_PE_config("NetCDFProcessing()"))
    else:
        config_dict["PE"]["PE"+str(i+1)] = "B2DROP()"
        config_dict["Workflow"]["PE"+str(i+1)+"_B2DROP"] = {
            'username': None,
            'password': None
        }

#Get input from first calculation
pe1_config_init = config_dict["Workflow"]["PE1_NetCDFProcessing"]

#Get input from second calculation
pe2_config_init = config_dict["Workflow"]["PE2_NetCDFProcessing"]

#Get input for b2drop
pe3_config_init = config_dict["Workflow"]["PE3_B2DROP"]

pe1_config_filled = {
                'indice_name': 'SU',
                'slice_mode': 'JJA',
                'var_name': 'tasmax',
                'in_files': '/tmp/tasmax_day_CNRM-CM6-1_highresSST-present_r21i1p1f2_gr_19500101-19991231_version_3.nc'
            }

pe2_config_filled = {'user_indice': 
                {
                    'indice_name':'SU',
                    'calc_operation': 'mean'
                },

                'slice_mode': 'year',
                'var_name': 'SU',
                'out_file': '/tmp/toto.nc'
                }

pe3_config_filled = {
    'username': 'FILL THIS SECTION',
    'password': 'FILL THIS SECTION'
}

pe1_config = merge_two_dicts(pe1_config_init, pe1_config_filled)
pe2_config = merge_two_dicts(pe2_config_init, pe2_config_filled)

#Create the dictionnary for the PE
config_dict["Workflow"] = [{"PE1_NetCDFProcessing":pe1_config,
                            "PE2_NetCDFProcessing":pe2_config,
                            "PE3_B2DROP":pe3_config_filled}]

with open(conf_filename, 'w') as outfile:  
    json.dump(config_dict, outfile)

with open(conf_filename) as inputfile:
    input_data = json.load(inputfile)

graph = pps.build_graph(conf_filename)

from dispel4py.new import simple_process
result = simple_process.process_and_return(graph, config_dict)
