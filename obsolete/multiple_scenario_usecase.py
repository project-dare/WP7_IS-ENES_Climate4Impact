from pywps.Process.Process import WPSProcess   

from os.path import expanduser
from mkdir_p import *
import dateutil.parser
from datetime import datetime
home = expanduser("~")
import json
#import PreProcess as pps
import owncloud
import io
from icclim import icclim
from icclim.util import read
import logging;
logging.debug("Something has been debugged")
from collections import OrderedDict
from dispel4py.provenance27 import *
from netCDF4 import Dataset

try:
    to_unicode = unicode
except NameError:
    to_unicode = str


class Process(WPSProcess):
    icclim_indice = read.get_icclim_indice_config()
    icclim_slice_mode = read.get_icclim_slice_mode()

    def __init__(self, icclim_indice=icclim_indice, icclim_slice_mode=icclim_slice_mode):
        # init process
        WPSProcess.__init__(self,
                            identifier="multiple_scenario_usecase", 
                            title="Multiple scenario workflow powered by dispel4py",
                            version = "1.0",
                            storeSupported = "true",
                            statusSupported = "true",
                            abstract="Build a workflow to perform indice calculation powered by icclim",
                            grassLocation =False)

        self.calc_operation = self.addLiteralInput(identifier = 'Calculation',
                                                     title = 'Indice Name to calculate',
                                                     type = type("String"),
                                                     default = "SU") 

        self.calc_operation.values = icclim_indice

        self.input_varName = self.addLiteralInput(identifier = 'Input Var Name',
                                                     title = 'Input variable name NetCDF file',
                                                     type = type("String"),
                                                     default = "tasmax") 


        self.timeRangeIn = self.addLiteralInput(identifier = 'timeRange', 
                                               title = 'Time range, e.g. 2010-01-01/2012-12-31. None means all dates in the file.',
                                               type="String",
                                                default = 'None')

        self.infiles = self.addLiteralInput(identifier = 'NetCDF Input File(s)',
                                                     title = 'NetCDF Input File(s)',
                                                     type = type("String"),
                                                     abstract="application/netcdf",
                                                     minOccurs=0,
                                                     maxOccurs=1024,
                                                     default = "/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r1i2p1_18510101_20051231.nc," +
                                                               "/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r2i2p1_18510101_20051231.nc," +
                                                               "/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r3i2p1_18510101_20051231.nc")

        self.slicemode = self.addLiteralInput(identifier = 'Slice Mode',
                                                     title = 'Temporal Slice Mode',
                                                     type = type("String"),
                                                     default = "year")   

        self.slicemode.values = icclim_slice_mode

        self.outputFileNameIn = self.addLiteralInput(identifier = 'outputFileName', 
                                                     title = 'Name of output netCDF file',
                                                     type="String",
                                                     default = 'out_icclim.nc')

        self.outputAvgRaster = self.addComplexOutput(identifier = "avgbuffer",
                                                     title = "AveragePlot",
                                                     formats = [
                                                        {"mimeType":"image/png"}
                                                     ])

        self.outputStdRaster = self.addComplexOutput(identifier = "stdbuffer",
                                                     title = "StandardDeviationPlot",
                                                     formats = [
                                                         {"mimeType":"image/png"}
                                                     ])

        self.b2drop_id = self.addLiteralInput(identifier = 'B2Drop ID',
                                              title = 'B2Drop ID',
                                              abstract = 'Enter your B2Drop ID',
                                              type=type("String"),
                                              default = 'ID')

        self.b2drop_token = self.addLiteralInput(identifier = 'B2Drop secret Token',
                                                 title = 'B2Drop secret Token',
                                                 abstract = 'Enter your B2Drop secret Token',
                                                 type=type("String"),
                                                 default = 'Token')

        self.opendapURL = self.addLiteralOutput(identifier = "opendapURL",title = "opendapURL");   

    def callback(self,message,percentage):
        self.status.set("Processing: [%s]" % message,percentage);
        
    def execute(self):

        def callback(a,b):
          self.callback(a,b)

        tmpFolderPath=os.getcwd()
        os.chdir(home)

        import time
        from ENES_usecase import PreProcess as pps

        self.status.set("Preparing....", 0)
        
        pathToAppendToOutputDirectory = "/WPS_"+self.identifier+"_" + datetime.datetime.now().strftime("%Y%m%dT%H%M%SZ")
        
        """ URL output path """
        fileOutURL  = os.environ['POF_OUTPUT_URL']  + pathToAppendToOutputDirectory+"/"
        
        """ Internal output path"""
        fileOutPath = os.environ['POF_OUTPUT_PATH']  + pathToAppendToOutputDirectory +"/"

        """ Create output directory """
        mkdir_p(fileOutPath)
        self.status.set("Starting.... Creating configuration.", 1)

        """ Get output filename """

        def merge_two_dicts(x, y):
            z = x.copy()   # start with x's keys and values
            z.update(y)    # modifies z with y's keys and values & returns None
            return z

        #config_structure["icclim"]["indice"].keys()
        #Loading the config structure for dispel4py from config_indice.json in icclim library
        conf_filename = "multiple_scenario.json"
        conf_filename_path = fileOutPath + conf_filename

        out_nc_file = self.outputFileNameIn.getValue()
        out_nc_file_path = fileOutPath + self.outputFileNameIn.getValue()

        config_dict = OrderedDict()

        config_dict["Workflow"] = OrderedDict() 

        infiles = self.infiles.getValue()
        number_nc = len(self.infiles.getValue())
        if type(infiles) is not list:
            list_nc = [infiles]
        else:
            list_nc = infiles

        config_scenario = OrderedDict()

        time_range = self.timeRangeIn.getValue()
        if(time_range == "None"):
            time_range = None
        else:
            startdate = dateutil.parser.parse(time_range.split("/")[0])
            stopdate  = dateutil.parser.parse(time_range.split("/")[1])
            time_range = [startdate,stopdate]

        indice_name = self.calc_operation.getValue()

        nblock = 0
        i=0
        npar = 0
        config_scenario["Block"+str(nblock)] = OrderedDict()
        for nc_file in list_nc:
            config_scenario["Block"+str(nblock)][npar] = OrderedDict()
            pe_name = "PE_calculation_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            config_scenario["Block"+str(nblock)][npar][pe_name] = OrderedDict()
            config_scenario["Block"+str(nblock)][npar][pe_name]["PE"] = "NetCDFProcessing()"
            config_scenario["Block"+str(nblock)][npar][pe_name]["config"] = OrderedDict({
                "indice_name" : indice_name,
                "in_files" : infiles,
                "var_name" : self.input_varName.getValue(),
                "slice_mode" : self.slicemode.getValue(),
                "time_range" : time_range,
                "out_unit" : "days",
                "out_file" : out_nc_file_path
            })
            config_scenario["Block"+str(nblock)][npar][pe_name]["input"] = "null"
            i+=1
            pe_name = "PE_Read"+"_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            config_scenario["Block"+str(nblock)][npar][pe_name] = OrderedDict()
            config_scenario["Block"+str(nblock)][npar][pe_name]["PE"] = "ReadNetCDF()"
            config_scenario["Block"+str(nblock)][npar][pe_name]["config"] = {}
            config_scenario["Block"+str(nblock)][npar][pe_name]["input"] = "PE_calculation_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            i+=1
            pe_name = "PE_Average"+"_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            config_scenario["Block"+str(nblock)][npar][pe_name] = OrderedDict()
            config_scenario["Block"+str(nblock)][npar][pe_name]["PE"] = "AverageData()"
            config_scenario["Block"+str(nblock)][npar][pe_name]["config"] = {}
            config_scenario["Block"+str(nblock)][npar][pe_name]["input"] = "PE_Read"+"_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            i+=1
            pe_name = "PE_StandardDeviation"+"_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            config_scenario["Block"+str(nblock)][npar][pe_name] = OrderedDict()
            config_scenario["Block"+str(nblock)][npar][pe_name]["PE"] = "StandardDeviation()"
            config_scenario["Block"+str(nblock)][npar][pe_name]["config"] = {}
            config_scenario["Block"+str(nblock)][npar][pe_name]["input"] = "PE_Read"+"_"+indice_name+"_"+str(nblock)+"_"+str(npar)
            i+=1
            npar+=1
            
        nblock+=1

        npar = 0

        config_scenario["Block"+str(nblock)] = OrderedDict()
        config_scenario["Block"+str(nblock)][npar] = OrderedDict()

        pe_avgplot_name = "PE_Average_Plot"+"_"+indice_name
        config_scenario["Block"+str(nblock)][npar][pe_avgplot_name] = OrderedDict()
        config_scenario["Block"+str(nblock)][npar][pe_avgplot_name]["PE"] = "CombineAndPlot()"
        config_scenario["Block"+str(nblock)][npar][pe_avgplot_name]["config"] = {}
        config_scenario["Block"+str(nblock)][npar][pe_avgplot_name]["input"] = OrderedDict()

        config_scenario["Block"+str(nblock)][npar][pe_avgplot_name]["input"] = []
        for ni in range(0, nblock):
            config_scenario["Block"+str(nblock)][npar][pe_avgplot_name]["input"].append("PE_Average"+"_"+indice_name+"_"+str(ni)+"_"+str(npar))

        pe_stdplot_name = "PE_StandardDeviation_Plot"+"_"+indice_name
        config_scenario["Block"+str(nblock)][npar][pe_stdplot_name] = OrderedDict()
        config_scenario["Block"+str(nblock)][npar][pe_stdplot_name]["PE"] = "CombineAndPlot()"
        config_scenario["Block"+str(nblock)][npar][pe_stdplot_name]["config"] = {}
        config_scenario["Block"+str(nblock)][npar][pe_stdplot_name]["input"] = OrderedDict()

        config_scenario["Block"+str(nblock)][npar][pe_stdplot_name]["input"] = []
        for ni in range(0, nblock):
            config_scenario["Block"+str(nblock)][npar][pe_stdplot_name]["input"].append("PE_StandardDeviation"+"_"+indice_name+"_"+str(ni)+"_"+str(npar))
            
        pe_name = "PE_B2DROP"+"_"+indice_name+"_"+str(nblock)
        config_scenario["Block"+str(nblock)][npar][pe_name] = OrderedDict()
        config_scenario["Block"+str(nblock)][npar][pe_name]["PE"] = "B2DROP()"
        config_scenario["Block"+str(nblock)][npar][pe_name]["config"] = OrderedDict({
            'username': self.b2drop_id.getValue(),
            'password': self.b2drop_token.getValue()
        })
        config_scenario["Block"+str(nblock)][npar][pe_name]["input"] = ["PE_Average_Plot"+"_"+indice_name, "PE_StandardDeviation_Plot"+"_"+indice_name]
        
        config_dict["Workflow"] = config_scenario

        with open(conf_filename_path, 'w') as outfile:  
            json.dump(config_dict, outfile)

        self.status.set("Creating Workflow...", 10)

        #-----------------------------------------------------------------------------------
        # Starting DARE dispel4py code section: to be externalized
        with open(conf_filename_path) as inputfile:
            input_data = json.load(inputfile, object_pairs_hook=OrderedDict)

        graph = pps.create_multiple_scenario_workflow(input_data)
        
        self.status.set("Setting up Provenance...", 20)

        #Store via service
        
        ProvenanceType.REPOS_URL='http://testbed.project-dare.eu/prov//workflowexecutions/insert'
        ProvenanceType.PROV_EXPORT_URL='http://testbed.project-dare.eu/prov//workflowexecutions/'

        #Store to local path
        ProvenanceType.PROV_PATH='./prov-files/'

        #Size of the provenance bulk before sent to storage or sensor
        ProvenanceType.BULK_SIZE=1        

        prov_config =  {
            'provone:User': "cc4idev", 
            's-prov:description' : "enes_multiple_scenarios",
            's-prov:workflowName': "enes_multiple_scenarios",
            's-prov:workflowType': "climate:preprocess",
                    's-prov:workflowId'  : "workflow process",
            's-prov:save-mode'   : 'service'         ,
            's-prov:WFExecutionInputs':  [{
                "url": "",
                "mime-type": "text/json",
                "name": "input_data"
                
            }],
                    # defines the Provenance Types and Provenance Clusters for the Workflow Components
            's-prov:componentsType' : 
            {self.calc_operation.getValue()+'_Spatial_MEAN': {'s-prov:type':(AccumulateFlow,),
                                's-prov:prov-cluster':'enes:Processor'},
            self.calc_operation.getValue()+'_Spatial_STD': {'s-prov:type':(AccumulateFlow,),
                                's-prov:prov-cluster':'enes:Processor'},
            self.calc_operation.getValue()+'_workflow': {'s-prov:type':(icclimInputParametersDataProvType,),
                                's-prov:prov-cluster':'enes:dataHandler'},
#             'PE_filter_bandpass': {'s-prov:type':(SeismoPE,),
#                                                     's-prov:prov-cluster':'seis:Processor'},
#             'StoreStream':    {'s-prov:prov-cluster':'seis:DataHandler',
#                                's-prov:type':(SeismoPE,)},
            }
#            's-prov:sel-rules': None
        }

        rid='JUP_ENES_PREPOC_'+getUniqueId()

        self.status.set("Initialising Provenance...", 25)

        #Initialise provenance storage to service:
        configure_prov_run(graph, 
                           provImpClass=(ProvenanceType,),
                           input=prov_config['s-prov:WFExecutionInputs'],
                           username=prov_config['provone:User'],
                           runId=rid,
                           description=prov_config['s-prov:description'],
                           workflowName=prov_config['s-prov:workflowName'],
                           workflowType=prov_config['s-prov:workflowType'],
                           workflowId=prov_config['s-prov:workflowId'],
                           save_mode=prov_config['s-prov:save-mode'],
                           componentsType=prov_config['s-prov:componentsType']
#                           sel_rules=prov_config['s-prov:sel-rules']
        )
        
        from dispel4py.workflow_graph import drawDot
        graph_filename = "workflow_graph.png"
        with open(graph_filename, 'wb') as f:
            f.write(drawDot(graph))        

        self.status.set("Executing Workflow...", 35)

        # Write JSON file
        from dispel4py.new import simple_process
        simple_process.process_and_return(graph, input_data)
        
        #------------------------ End DARE dispel4py section

        out_file_name = out_nc_file
        
        """ Set output """
        url = fileOutURL+"/"+out_file_name;
        self.opendapURL.setValue(url);
        self.outputAvgRaster.setValue('test.png');
        self.outputStdRaster.setValue('test.png');
        self.status.set("ready",100);


class icclimInputParametersDataProvType(ProvenanceType):
    def __init__(self):
        ProvenanceType.__init__(self)
        self.addNamespacePrefix("enes","https://climate4impact.eu")
        self.addNamespacePrefix("var","http://openprovenance.org#")
    
    ''' extracts xarray metadata '''
    def extractItemMetadata(self, data, output_port):
         
        
        try:            
            nc_meta = []
            
            ''' cycle throug all attributes, dimensions and variables '''
            for n in range(len(data.values())):
                filename = data.values()[n]['in_files']
                ds = Dataset(filename, "r")
                netcdfattr = OrderedDict()
                netcdfattr['filename'] = filename.split('/')[-1]
                for name in ds.ncattrs():
                    netcdfattr[name] = str(getattr(ds,name))
                nc_meta.append(netcdfattr)

            return nc_meta
                             
        except Exception:
            self.log("Applying default metadata extraction"+str(traceback.format_exc()))
            self.error=self.error+"Applying default metadata extraction:"+str(traceback.format_exc())
            return super(icclimInputParametersDataProvType, self).extractItemMetadata(data,output_port);
        
        
