import json
import pdb
import pe_enes as ps
from dispel4py.workflow_graph import WorkflowGraph

def build_graph(json_path):

    with open(json_path) as json_data:
        input_C4I = json.load(json_data)
    name_pe = input_C4I['PE'].keys()
    pe_class = input_C4I['PE']
    name_pe.sort()

    #Create the PE defined by JSON files from C4I
    for pe in pe_class:
        method2eval = "ps."+pe_class[pe]
        name2eval = pe+"_"+pe_class[pe][0:-2]

        try:
            exec(pe+"="+method2eval)
            exec(pe+".name=name2eval")
            #pdb.set_trace()
        except AssertionError as error:
            print(error)
            print('Unknown Function from pe_enes module')

    graph = WorkflowGraph()

    streamProducer = ps.StreamProducer()
    streamProducer.name = "Workflow"

    i=0
    graph.connect(streamProducer, 'output', eval(name_pe[i]), 'input')
    while i<len(name_pe[0:-1]):
        graph.connect(eval(name_pe[i]), 'output', eval(name_pe[i+1]), 'input')
        i+=1

    return graph

def create_multiple_scenario_workflow(config_json):

    graph = WorkflowGraph()
    streamProducer = ps.StreamProducer()
    streamProducer.name = "Workflow"

    list_block = config_json["Workflow"].keys()
    nblock = len(list_block)

    import logging

    for block in list_block:
        list_par = config_json["Workflow"][block].keys()
        npar = len(list_par)
        for par in list_par:
            list_pe = config_json["Workflow"][block][par].keys()
            for key in list_pe:
                pe_class = config_json["Workflow"][block][par][key]["PE"]
                method2eval = "ps."+pe_class
                name2eval = key
            
                try:
                    exec(key+"="+method2eval)
                    exec(key+".name=name2eval")
                except AssertionError as error:
                    print(error)
                    print('Unknown Function from pe_enes module')
                    
                input_name = config_json["Workflow"][block][par][key]["input"]
                if (input_name == "null") :
                    graph.connect(streamProducer, 'output', eval(key), 'input')
                else :
                    logging.debug(block)
                    logging.debug(par)
                    logging.debug(key)
                    logging.debug(input_name)
                    input_pe_name = config_json["Workflow"][block][par][input_name]["PE"]
                    try:
                        exec(input_name+"=ps."+input_pe_name)
                        exec(input_name+".name=input_name")
                    except AssertionError as error:
                        print(error)
                        print('Unknown Function from pe_enes module')

                    graph.connect(eval(input_name), 'output', eval(key), 'input')
                    
    return graph

    # read_r1i2p1 = ps.ReadNetCDF()
    # read_r1i2p1.name = "read_SU_r1i2p1"

    # mean_calculation_r1i2p1 = ps.AverageData()
    # mean_calculation_r1i2p1.name = "mean_r1i2p1"

    # std_calc_r1i2p1 = ps.StandardDeviation()
    # std_calc_r1i2p1.name = "std_r1i2p1"


    # ###############################
    # #Workflow for r2i2p1 simulation
    # ###############################    
    # su_calculation_r2i2p1 = ps.NetCDFProcessing()
    # su_calculation_r2i2p1.name = 'SU_calculation_r2i2p1'

    # read_r2i2p1 = ps.ReadNetCDF()
    # read_r2i2p1.name = "read_SU_r2i2p1"

    # mean_calculation_r2i2p1 = ps.AverageData()
    # mean_calculation_r2i2p1.name = "mean_r2i2p1"

    # std_calc_r2i2p1 = ps.StandardDeviation()
    # std_calc_r2i2p1.name = "std_r2i2p1"


    # ###############################
    # #Workflow for r3i2p1 simulation
    # ###############################    
    # su_calculation_r3i2p1 = ps.NetCDFProcessing()
    # su_calculation_r3i2p1.name = 'SU_calculation_r3i2p1'

    # read_r3i2p1 = ps.ReadNetCDF()
    # read_r3i2p1.name = "read_SU_r3i2p1"

    # mean_calculation_r3i2p1 = ps.AverageData()
    # mean_calculation_r3i2p1.name = "mean_r3i2p1"

    # std_calc_r3i2p1 = ps.StandardDeviation()
    # std_calc_r3i2p1.name = "std_r3i2p1"


    # ###############################
    # #Workflow to combine and plot the calculation together
    # ###############################
    # combine_std = ps.CombineAndPlot()
    # combine_std.name = "SU_Spatial_STD"   

    # combine_mean = ps.CombineAndPlot()
    # combine_mean.name = "SU_Spatial_MEAN"   


    # ###############################
    # #Workflow to combine and plot the calculation together
    # ###############################   
    # b2drop = ps.B2DROP()
    # b2drop.name = "b2drop_storage"

    # ###############################
    # #Workflow starts here
    # ###############################
    # graph = WorkflowGraph()

    # #Calculation for r1i2p1
    # graph.connect(streamProducer, 'output', su_calculation_r1i2p1, 'input')
    # graph.connect(su_calculation_r1i2p1, 'output', read_r1i2p1, 'input')
    # graph.connect(read_r1i2p1, 'output', mean_calculation_r1i2p1, 'input')
    # graph.connect(read_r1i2p1, 'output', std_calc_r1i2p1, 'input')

    # #Calculation for r2i2p1
    # graph.connect(streamProducer, 'output', su_calculation_r2i2p1, 'input')
    # graph.connect(su_calculation_r2i2p1, 'output', read_r2i2p1, 'input')  
    # graph.connect(read_r2i2p1, 'output', mean_calculation_r2i2p1, 'input')
    # graph.connect(read_r2i2p1, 'output', std_calc_r2i2p1, 'input')

    # #Calculation for r3i2p1
    # graph.connect(streamProducer, 'output', su_calculation_r3i2p1, 'input')
    # graph.connect(su_calculation_r3i2p1, 'output', read_r3i2p1, 'input')
    # graph.connect(read_r3i2p1, 'output', mean_calculation_r3i2p1, 'input')
    # graph.connect(read_r3i2p1, 'output', std_calc_r3i2p1, 'input')

    # ###############################
    # #We combine the standard deviation and plot it together. Same for the average.
    # ###############################
    # graph.connect(std_calc_r1i2p1, 'output', combine_std, 'var1')
    # graph.connect(std_calc_r2i2p1, 'output', combine_std, 'var2')
    # graph.connect(std_calc_r3i2p1, 'output', combine_std, 'var3')

    # graph.connect(mean_calculation_r1i2p1, 'output', combine_mean, 'var1')
    # graph.connect(mean_calculation_r2i2p1, 'output', combine_mean, 'var2')
    # graph.connect(mean_calculation_r3i2p1, 'output', combine_mean, 'var3')

    # ###############################
    # #We store all the results on b2drop
    # ###############################
    # graph.connect(combine_std, 'output', b2drop, 'input')
    # graph.connect(combine_mean, 'output', b2drop, 'input')

    # return graph
