from icclim import icclim
from icclim.util import read
from collections import OrderedDict
import pdb
import json
import PreProcess as pps
from dispel4py.workflow_graph import WorkflowGraph
from dispel4py.base import IterativePE, ProducerPE, ConsumerPE
from dispel4py.core import GenericPE

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

conf_filename = "multiple_scenario.json"

config_dict = OrderedDict()

config_dict["SU_workflow"] = OrderedDict() 

list_nc = ["/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r1i2p1_18510101_20051231.nc","/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r2i2p1_18510101_20051231.nc","/tmp/tasmax_day_CSIRO-Mk3L-1-2_historical_r3i2p1_18510101_20051231.nc"]

list_scenario = ["SU_calculation_r1i2p1", "SU_calculation_r2i2p1", "SU_calculation_r3i2p1"]
config_scenario = config_dict['SU_workflow']

i=0
for nc_file in list_nc:
    config_scenario[list_scenario[i]] = {"indice_name" : "SU",
            "in_files" : nc_file,
            "var_name" : "tasmax",
            "slice_mode" : "JJA",
            "out_unit" : "days",
            "out_file" : None
                    }
    i+=1

config_dict['SU_workflow'] = [config_scenario]

with open(conf_filename, 'w') as outfile:  
    json.dump(config_dict, outfile)


with open(conf_filename) as inputfile:
    input_data = json.load(inputfile)

graph = pps.create_multiple_scenario_workflow()

from dispel4py.new import simple_process
result = simple_process.process_and_return(graph, input_data)

from dispel4py.workflow_graph import drawDot
with open('graph_test.png', 'wb') as f:
   f.write(drawDot(graph)) 