from icclim import icclim
import numpy as np
import netCDF4
import matplotlib.pyplot as plt
import matplotlib
import sys
import os
import datetime
import cftime

print("python: ",sys.version)
print("numpy: ", np.__version__)
print("netCDF4: ", netCDF4.__version__)
print("matplotlib: ", matplotlib.__version__)

# studied period
dt1 = datetime.datetime(2015,1,1)
dt2 = datetime.datetime(2100,12,31)

out_f = 'su_icclim.nc'
filenames = ['http://vesg.ipsl.upmc.fr/thredds/dodsC/cmip6/ScenarioMIP/IPSL/IPSL-CM6A-LR/ssp585/r1i1p1f1/day/tasmax/gr/v20190903/tasmax_day_IPSL-CM6A-LR_ssp585_r1i1p1f1_gr_20150101-21001231.nc']

icclim.indice(indice_name='SU', in_files=filenames, var_name='tasmax', slice_mode='JJA', time_range=[dt1, dt2], transfer_limit_Mbytes=200, out_file=out_f)

matplotlib.use('agg')

nc = netCDF4.Dataset(out_f)

time = nc.variables['time']
var = nc.variables['SU']

time = cftime.num2date(time[:], time.units, time.calendar)
year_list = np.array([t.year for t in time])

plt.figure()

# Calculate spatial average
var = np.reshape(var, (var.shape[0], -1))
result = np.mean(var, axis=1)

plt.plot(year_list, result, label='SU')
plt.legend()
plt.xlabel('Year')
plt.ylabel('Number')
plt.grid()

name_fig = "su_icclim.png"
plt.savefig("./"+name_fig)




